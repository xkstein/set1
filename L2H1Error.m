function [L2Error,H1Error] = L2H1Error(ksi,mesh)
    u = @(x) sin(pi*x);
    du = @(x) pi*cos(pi*x);
    
    u_uh = UMinusUh(ksi,0,mesh);
    du_uh = dUMinusUh(ksi,0,mesh);

    L2Error = sqrt(Simpson1D(u_uh.^2,mesh));
    H1Error = sqrt(Simpson1D((u_uh.^2 + du_uh.^2),mesh));
    %L2Error = sqrt(Quad1D(u_uh.^2,mesh));
    %H1Error = sqrt(Quad1D((u_uh.^2 + du_uh.^2),mesh));
end

function u_uh = UMinusUh(ksi,t,mesh)
    u = @(x,t) sin(pi*x) + (t*0);

    u_uh = zeros([length(ksi) 1]);
    for i = 1:length(ksi)
        x_i = mesh.h * (i-1);
        u_uh(i) = u(x_i,t) - ksi(i);
    end
end

function du_uh = dUMinusUh(ksi,t,mesh)
    du = @(x,t) pi*cos(pi*x) + (t*0);
    
    du_uh = zeros([length(ksi) 1]);
    for i = 1:length(ksi)
        x_i = mesh.h * (i-1);
        
        switch i
            case 1
                du_uh(i) = du(x_i,t) - (ksi(2)-ksi(1))/(mesh.h);
            case length(ksi)
                du_uh(i) = du(x_i,t) - (ksi(end)-ksi(end-1))/(mesh.h);
            otherwise
                du_uh(i) = du(x_i,t) - (ksi(i+1)-ksi(i-1))/(2*mesh.h);
        end
    end
end

% This uses the trapezoid rule
function integral = Quad1D(inner,mesh)
    intSum = inner(1) + inner(end);
    for i = 2:length(inner)-1
        intSum = intSum + (2 * inner(i));
    end
    integral = mesh.h*intSum/2;
end

% This uses simpsons rule
function integral = Simpson1D(inner,mesh)
    simpsonSum = inner(1) + inner(end) + (4 * inner(2));
    for i = 4:2:length(inner)-1
        simpsonSum = simpsonSum + (4 * inner(i)) + (2 * inner(i-1));
    end
    integral = simpsonSum * mesh.h/3;
end
