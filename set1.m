function set1(n)
    %clc
    %close all

    % defining width of slices and number of nodes
    %n = 3;
    h = 1/(n+1);
    x_i = @(i) h*(i);

    mesh.n = n;
    mesh.h = h;

    switch 1
        case 1
            a = @(x) 1;
            bx = 1;
            f = @(x) (pi^2+1)*sin(pi*x);
        case 2
            % part 2 isn't working, god dammit
            a = @(x) 1/(1+10*x^2);
            bx = 0;
            %f = @(x) 20*x*pi*cos(pi*x)*a(x)^2+pi^2*sin(pi*x)*a(x);
            f = @(x) (pi^2*sin(pi*x))/(1+10*x^2) + (20*x*pi*cos(pi*x))/((1+10*x^2)^2);
    end

    % Making the stiffness matrix
    localA = [[1 -1]
              [-1 1]];

    A = zeros(n);
    A(1,1) = a(0)/h;
    A(n,n) = a(x_i(n))/h;
    for i = 1:n-1
        A(i:i+1,i:i+1) = A(i:i+1,i:i+1) + (a(x_i(i))/h) * localA;
    end

    % Making the B matrix
    if (bx ~= 0)
        B = zeros(n);
        localB = h*[[2/3 1/6]
                  [1/6 0]];
        B(n,n) = bx*2*h/3;
        for i = 1:n-1
            B(i:i+1,i:i+1) = B(i:i+1,i:i+1) + bx * localB;
        end

        A = A + B;
    end

    % Making the load vector
    b = zeros([n 1]);
    for i = 1:n
        b(i) = f(x_i(i))*h;
    end

    % Defining and solving for ksi, which contains the value of the nodes
    ksi=zeros([n 1]);
    ksi = linsolve(A,b);

    % Function for finding the L2 and H1 error
    [L2Error,H1Error] = L2H1Error(ksi,mesh);
    fprintf('| 1/%i | %f | %f |\n',n+1,L2Error,H1Error); 

    X = h:h:1-h;
    plot(X,ksi)
    clear
end
